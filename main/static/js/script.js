$(document).ready(function () {
    $('.move-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });

    $('.move-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });

    $(".accordion_header").click(function(e){
        if ($(e.target).is("p")){
            return;
        }else {
            if($(this).is(".accordion_header.active")){
                $(this).removeClass("active");
            } else{
                $(this).addClass("active");
            }
        }
    });
});